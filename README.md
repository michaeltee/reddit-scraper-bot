# Reddit Scrape and Notify Bot 1.0

Written by Michael Taylor, August 2014.

### What does this script do?

* Scrape a subreddit and send notifications to a list of users if there is new content.

### How do I get set up?

* Summary of project
* Dependencies
* Configuration Files
* Database configuration
* Get it running
* Questions?

### Summary of project

This script was created due the desire of the users of a subreddit that I moderate to receive direct messages whenever a new post is made, similar to how Facebook alerts members of a group when someone posts in it.  Out of the box it will pull down the five newest submissions whenever it is run, compare them to a list of submissions in a database, and send a notification out to a group of users if the post is determined to be new.  It's very simple and requires very little overhead to function properly.  I leverage the fantastic PRAW module to interface with Reddit as well as the MySQL Python/Connector driver for database operations, though it would be easy enough to change this script to connect to any ODBC-compliant database.

### Dependencies

* Python 2.x (tested with 2.7.6)
* PRAW - Python Reddit API Wrapper - [PyPI](https://pypi.python.org/pypi/praw) | [ReadTheDocs](https://praw.readthedocs.org)
* MySQL - [Oracle](http://dev.mysql.com/downloads)
* MySQL Connector/Python - [PyPI](https://pypi.python.org/pypi/mysql-connector-python) | [Oracle](http://dev.mysql.com/downloads/connector/python)
* Any OS should conceivably work - I tested/run it on an Ubuntu Server 14.04 LTS instance

### Configuration files

* **notifier.cfg** - contains script runtime settings and host configuration info
* **recipients** - line-delimited text file containing the usernames you'd like to notify

### Database configuration

The database configuration out of the box assumes three things:

1. You're using MySQL.
2. You've got the MySQL Connector/Python package installed.
3. Your database contains a table named **submissions**.  You can easily change this table name in the code, but out of the box this is what we look for.  

DB schema:

    CREATE TABLE `submissions` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `link_text` longtext,
      `link_hash` varchar(100) DEFAULT NULL,
      PRIMARY KEY (`id`)
    )

### Get it running

1. Download and install the required packages listed in the dependencies section.
2. Configure your database as I've listed above.
3. Edit the **notifier.cfg** file to reflect your host- and user-specific settings.
4. Edit the **recipients** file to contain a line-delimited list of all the users you want this tool to reach (I recommend starting out with just one or two test users to avoid spamming folks if something goes wrong).
5. Test it out - I suggest a test subreddit to start with ([/r/test](http://reddit.com/r/test) works great for this).
6. Send it to production when you're done fine-tuning your subreddit-specific settings - I stuck mine on a crontab that runs every minute and compares 5 entries at a time since that was enough for the traffic on my subreddit.

### Questions
Shoot me an email - michael@michaeltaylor.io.  Pull requests are always appreciated!