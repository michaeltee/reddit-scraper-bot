# Reddit notification tool.
# Michael Taylor, August 2014

import ConfigParser
import praw
import hashlib
import datetime
import mysql.connector

# Read notifier.cfg.
config = ConfigParser.ConfigParser()
# Use a fully-qualified filepath if you are running as a cron job.
config.read('notifier.cfg')
cfg = dict(config.items('config'))

# Create DB connection.
cxn = mysql.connector.connect(host=cfg['db_host'], 
    user=cfg['db_user'], password=cfg['db_pwd'], 
    database=cfg['db'])

# Configure credentials and subreddit for PRAW to use.
user = cfg['msg_bot_user']
pwd = cfg['msg_bot_pwd']
sub = cfg['subreddit']

# Take a line-delimited file of recipients and make a sender list.
# As above, use a full filepath if you're using a crontab.
recipients = open('recipients').read().splitlines()

# Provide descriptive string to comply with Reddit API rules.
r = praw.Reddit('User Agent: bot that scrapes r/%s for new posts and sends a '
                'notification to users, by u/mt_ftw - v1.1' % sub)

def check_db(link, hash, timestamp, author):
    """#Checks DB for the link, sends a notification for two conditions:
        1) Link hash + timestamp combo are not present in DB.
        2) Message target is not the author.
    """
    c = cxn.cursor(buffered=True)

    # Hash the URL that's returned and append timestamp for uniqueness.
    prepared_hash = hash + str(timestamp)

    select_query = ("SELECT link_text FROM submissions WHERE link_hash = %s")

    c.execute(select_query, (prepared_hash,))
    results = c.fetchall()

    # If there's nothing in the DB that matches, write to DB.
    if len(results) == 0:
        # print 'NO RESULT FOR %s, SENDING NOTIFICATION AND WRITING TO DB' % link
      
        insert_data = ("INSERT INTO submissions "
                        "(link_text, link_hash) "
                        "VALUES (%s, %s)")
      
        c.execute(insert_data, (link, prepared_hash))
        cxn.commit()

        # Check if the target is the author, then send if false.
        for target in recipients:
            # print target, author
            if cmp(target, str(author)):
                r.send_message(target, 'New post alert from %s' % sub, '%s has made a new post - follow the link here %s' % (author, link))
   
    # If there are matches, do nothing. Print the link for debugging if you want.
    elif len(results) > 0:
        # print 'RESULT FOUND FOR %s, SKIPPING DB INSERT AND NOTIFY' % link
        pass

# Fire it up.
r.login(user, pwd)
subreddit = r.get_subreddit(sub)

# Change the limit as necessary, 5 was enough for my subreddit.
for submission in subreddit.get_new(limit=5):
    check_hash = hashlib.sha256(submission.permalink).hexdigest()
    check_db(submission.permalink, check_hash, submission.created, 
            submission.author)